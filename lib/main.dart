import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mobx_architecture/data/api/api_services.dart';
import 'package:flutter_mobx_architecture/data/local/prefs/shared_prefs.dart';
import 'package:flutter_mobx_architecture/foundation/debug/mobx_spy.dart';
import 'package:flutter_mobx_architecture/generated/entities.g.dart';
import 'package:flutter_mobx_architecture/presentation/app/app_main.dart';

import 'presentation/app/app_injector.dart';

Future<void> main() async {
  /// Init to load native code
  WidgetsFlutterBinding.ensureInitialized();

  /// Init to load saved locale
  await EasyLocalization.ensureInitialized();

  /// Init singleton of shared preferences
  await sharedPrefs.init();

  if (kDebugMode) {
    /// Enable api debugging
    API.shared.debug = true;
  }

  /// Init object mapper
  Entities.register();

  // Mobx Debug
  registerMobXSpy();

  /// Insert dependencies
  AppInjector.registerDependencies();

  /// Set status bar color
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

  /// Run app
  await application();
}
