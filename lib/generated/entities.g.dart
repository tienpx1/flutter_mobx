// DO NOT EDIT. This is code generated via flutter_gen

import 'package:pt_flutter_object_mapper/pt_flutter_object_mapper.dart';
import 'package:flutter_mobx_architecture/models/user.dart';
import 'package:flutter_mobx_architecture/models/article.dart';
import 'package:flutter_mobx_architecture/data/api/api_output.dart';
import 'package:flutter_mobx_architecture/data/api/api+user.dart';
import 'package:flutter_mobx_architecture/data/api/api+news.dart';
import 'package:flutter_mobx_architecture/data/api/api_error.dart';

class Entities {
  Entities._();
  
  static void register() {
    Mappable.factories[User] = () => User();
    Mappable.factories[Article] = () => Article();
    Mappable.factories[APIOutput] = () => APIOutput();
    Mappable.factories[UserOutput] = () => UserOutput();
    Mappable.factories[NewsOutput] = () => NewsOutput();
    Mappable.factories[APIError] = () => APIError();
  }
}
