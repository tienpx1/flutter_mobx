// DO NOT EDIT. This is code generated via flutter_gen
// ignore_for_file: non_constant_identifier_names, camel_case_types

import 'package:easy_localization/easy_localization.dart';

class i18n {
  static String get dashboard => 'dashboard'.tr();
  static String get dashboard_bottom_bar_menu => 'dashboard.bottom_bar_menu'.tr();
  static String get dashboard_bottom_bar_menu_home_title => 'dashboard.bottom_bar_menu.home_title'.tr();
  static String get dashboard_bottom_bar_menu_message_title => 'dashboard.bottom_bar_menu.message_title'.tr();
  static String get dashboard_bottom_bar_menu_dealer_title => 'dashboard.bottom_bar_menu.dealer_title'.tr();
  static String get dashboard_bottom_bar_menu_settings_title => 'dashboard.bottom_bar_menu.settings_title'.tr();
}