// DO NOT EDIT. This is code generated via flutter_gen

import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';
import 'package:flutter_mobx_architecture/presentation/app/app_pages.dart';

class AppRouter {
  void back({int? id}) {
    return Get.back(id: id);
  }

  void backToRoot({int? id}) {
    Get.until((route) => route.isFirst, id: id);
  }

  Future? toDemoList({int? id}) {
    return Get.toNamed(Routes.DEMO_LIST, id: id);
  }

  Future? offAllDemoList({int? id}) {
    return Get.offAllNamed(Routes.DEMO_LIST, id: id);
  }

  Future? offAndToDemoList({int? id}) {
    return Get.offAndToNamed(Routes.DEMO_LIST, id: id);
  }

  Future? offDemoList({int? id}) {
    return Get.offNamed(Routes.DEMO_LIST, id: id);
  }

  Future? offUntilDemoList({int? id}) {
    return Get.offNamedUntil(Routes.DEMO_LIST, (_) => false);
  }

  Future? toDemoLogin({int? id}) {
    return Get.toNamed(Routes.DEMO_LOGIN, id: id);
  }

  Future? offAllDemoLogin({int? id}) {
    return Get.offAllNamed(Routes.DEMO_LOGIN, id: id);
  }

  Future? offAndToDemoLogin({int? id}) {
    return Get.offAndToNamed(Routes.DEMO_LOGIN, id: id);
  }

  Future? offDemoLogin({int? id}) {
    return Get.offNamed(Routes.DEMO_LOGIN, id: id);
  }

  Future? offUntilDemoLogin({int? id}) {
    return Get.offNamedUntil(Routes.DEMO_LOGIN, (_) => false);
  }

  Future? toDemoLoadMore({int? id}) {
    return Get.toNamed(Routes.DEMO_LOAD_MORE, id: id);
  }

  Future? offAllDemoLoadMore({int? id}) {
    return Get.offAllNamed(Routes.DEMO_LOAD_MORE, id: id);
  }

  Future? offAndToDemoLoadMore({int? id}) {
    return Get.offAndToNamed(Routes.DEMO_LOAD_MORE, id: id);
  }

  Future? offDemoLoadMore({int? id}) {
    return Get.offNamed(Routes.DEMO_LOAD_MORE, id: id);
  }

  Future? offUntilDemoLoadMore({int? id}) {
    return Get.offNamedUntil(Routes.DEMO_LOAD_MORE, (_) => false);
  }
}
