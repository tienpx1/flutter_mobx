import 'package:flutter_mobx_architecture/data/api/api+news.dart';
import 'package:flutter_mobx_architecture/data/api/api_services.dart';
import 'package:flutter_mobx_architecture/models/article.dart';
import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';

class NewsRepository {
  Future<PagingInfo<Article>> getNews({int page = 1}) {
    return API.shared
        .getNews(NewsInput(page: page))
        .then((result) => PagingInfo(page, result.articles, page * 20 < result.totalResults));
  }
}
