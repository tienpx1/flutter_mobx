import 'package:flutter_mobx_architecture/data/api/api+user.dart';
import 'package:flutter_mobx_architecture/data/api/api_services.dart';
import 'package:flutter_mobx_architecture/models/user.dart';
import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';

class UserRepository {
  Future<PagingInfo<User>> getUsers({int page = 1}) async {
    return API.shared
        .getUsers(UserInput(page: page))
        .then((result) => PagingInfo(page, result.users, page * 30 < result.count));
  }
}
