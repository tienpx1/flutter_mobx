import 'package:flutter_mobx_architecture/data/repositories/login_repository.dart';
import 'package:flutter_mobx_architecture/data/repositories/news_repository.dart';
import 'package:flutter_mobx_architecture/data/repositories/user_repository.dart';
import 'package:flutter_mobx_architecture/models/article.dart';
import 'package:flutter_mobx_architecture/models/user.dart';
import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';

abstract class AppRepository {
  Future<PagingInfo<Article>> getNews({int page = 1});

  Future<PagingInfo<User>> getUsers({int page = 1});

  Future<void> login({required String email, required String password});
}

class AppRepositoryImpl extends AppRepository with NewsRepository, LoginRepository, UserRepository {}
