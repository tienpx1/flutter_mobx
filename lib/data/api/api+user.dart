// ignore_for_file: file_names

import 'package:flutter_mobx_architecture/models/user.dart';
import 'package:pt_flutter_architecture/pt_api_service.dart';
import 'package:pt_flutter_object_mapper/pt_flutter_object_mapper.dart';

import 'api_input.dart';
import 'api_output.dart';
import 'api_services.dart';
import 'api_urls.dart';

class UserInput extends APIInput {
  UserInput({required int page})
      : super(APIUrls.user,
            params: {
              "per_page": 30,
              "page": page,
            },
            requireAccessToken: false,
            httpMethod: HttpMethod.get);
}

class UserOutput extends APIOutput {
  List<User> users = [];
  int count = 0;

  @override
  void mapping(Mapper map) {
    super.mapping(map);
    map<User>("data.documents", users, (v) => users = v);
    map("data.count", count, (v) => count = v);
  }
}

extension UserAPI on API {
  Future<UserOutput> getUsers(UserInput input) {
    return requestFuture(input);
  }
}
