import 'package:flutter_mobx_architecture/presentation/app/app_flavors.dart';

class APIUrls {
  static final endpoint = Flavors.shared.endpoint;
  static String news = "$endpoint/v2/everything";
  static String user = "https://api.aniapi.com/v1/user";
}
