import 'package:pt_flutter_object_mapper/pt_flutter_object_mapper.dart';

class User with Mappable {
  String username = "";

  @override
  void mapping(Mapper map) {
    map("username", username, (v) => username = v);
  }
}
