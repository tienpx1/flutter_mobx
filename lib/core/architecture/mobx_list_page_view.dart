import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_list_page.dart';

class MobXListPageView<Item> extends StatelessWidget {
  final int scrollOffset;
  final MobXListPage<Item> store;
  final Widget child;
  final Widget? loadingIndicator;
  final Widget? reloadingIndicator;
  final Widget? loadingMoreIndicator;
  final Widget? emptyView;

  const MobXListPageView({
    Key? key,
    required this.child,
    this.scrollOffset = 0,
    this.emptyView,
    this.loadingIndicator,
    this.loadingMoreIndicator,
    this.reloadingIndicator,
    required this.store,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (store.onRefresh != null) {
      return NotificationListener<ScrollNotification>(
        child: RefreshIndicator(
          child: Column(
            children: [
              _buildLoadingIndicator(),
              _buildReLoadingIndicator(),
              _buildEmptyView(),
              Expanded(child: child),
              _buildLoadingMoreIndicator()
            ],
          ),
          onRefresh: () async => store.onRefresh!(),
        ),
        onNotification: _handleLoadMoreScroll,
      );
    } else {
      return NotificationListener<ScrollNotification>(
        child: Observer(
          builder: (_) {
            return Column(
              children: [
                _buildLoadingIndicator(),
                _buildEmptyView(),
                Expanded(child: child),
                _buildLoadingMoreIndicator()
              ],
            );
          },
        ),
        onNotification: _handleLoadMoreScroll,
      );
    }
  }

  Widget _buildLoadingIndicator() {
    return Observer(builder: (_) {
      if (store.isLoading) {
        if (loadingIndicator == null) {
          return Container(
            width: 55,
            height: 55,
            padding: const EdgeInsets.all(15),
            child: const CircularProgressIndicator(
              strokeWidth: 2,
            ),
          );
        } else {
          return loadingIndicator!;
        }
      }
      return Container();
    });
  }

  Widget _buildReLoadingIndicator() {
    return Observer(builder: (_) {
      if (store.isRefreshing) {
        if (reloadingIndicator != null) {
          return reloadingIndicator!;
        }
      }
      return Container();
    });
  }

  Widget _buildEmptyView() {
    return Observer(builder: (_) {
      if (store.isEmpty) {
        return emptyView ?? Container();
      }
      return Container();
    });
  }

  Widget _buildLoadingMoreIndicator() {
    return Observer(builder: (_) {
      if (store.isLoadingMore) {
        if (loadingMoreIndicator == null) {
          return Container(
            width: 55,
            height: 55,
            padding: const EdgeInsets.all(15),
            child: const CircularProgressIndicator(
              strokeWidth: 2,
            ),
          );
        } else {
          return loadingMoreIndicator!;
        }
      }
      return Container();
    });
  }

  bool _handleLoadMoreScroll(ScrollNotification notification) {
    if (notification is ScrollUpdateNotification) {
      if (notification.metrics.pixels - notification.metrics.maxScrollExtent > scrollOffset) {
        if (!store.isLoadingMore) {
          store.onLoadMore();
        }
      }
    }
    return false;
  }
}
