import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_list.dart';

class MobXListView extends StatelessWidget {
  final MobxList store;
  final Widget child;
  final Widget? loadingIndicator;
  final Widget? reloadingIndicator;
  final Widget? emptyView;

  const MobXListView({
    Key? key,
    required this.child,
    required this.store,
    this.loadingIndicator,
    this.reloadingIndicator,
    this.emptyView,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (store.onRefresh != null) {
      return RefreshIndicator(
        child: Column(
          children: [_buildLoadingIndicator(), _buildReLoadingIndicator(), _buildEmptyView(), Expanded(child: child)],
        ),
        onRefresh: () async => store.onRefresh!(),
      );
    }
    return Column(
      children: [
        _buildLoadingIndicator(),
        _buildEmptyView(),
        Expanded(child: child),
      ],
    );
  }

  Widget _buildLoadingIndicator() {
    return Observer(builder: (_) {
      if (store.isLoading) {
        if (loadingIndicator == null) {
          return Container(
            width: 55,
            height: 55,
            padding: const EdgeInsets.all(15),
            child: const CircularProgressIndicator(
              strokeWidth: 2,
            ),
          );
        } else {
          return loadingIndicator!;
        }
      }
      return Container();
    });
  }

  Widget _buildReLoadingIndicator() {
    return Observer(builder: (_) {
      if (store.isRefreshing) {
        if (reloadingIndicator != null) {
          return reloadingIndicator!;
        }
      }
      return Container();
    });
  }

  Widget _buildEmptyView() {
    return Observer(
      builder: (context) {
        if (store.isEmpty) {
          return emptyView ?? Container();
        }
        return Container();
      },
    );
  }
}
