class GridItem<T> {
  T data;
  bool isSelected = false;

  GridItem({required this.data, this.isSelected = false});

  GridItem<T> copyWith({T? data, bool isSelected = false}) {
    return GridItem(data: data ?? this.data, isSelected: isSelected);
  }
}
