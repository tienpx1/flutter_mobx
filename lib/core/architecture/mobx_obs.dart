import 'package:flutter/widgets.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class Obs extends StatelessWidget {
  final Widget Function(BuildContext context) builder;

  const Obs(this.builder, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Observer(builder: builder);
}
