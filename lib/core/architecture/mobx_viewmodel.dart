import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';

abstract class MobXViewModel<A, B> extends GetxController {
  final String? tag = null;

  A get repo => GetInstance().find<A>(tag: tag)!;

  B get navigator => GetInstance().find<B>(tag: tag)!;
  final bag = DisposeBag();

  @override
  void dispose() {
    Get.log('"${runtimeType.toString()}" disposed');
    bag.clear();
    super.dispose();
  }
}

abstract class MobXViewModelWithArgs<A, B, C> extends GetxController {
  final String? tag = null;
  late C args;

  A get repo => GetInstance().find<A>(tag: tag)!;

  B get navigator => GetInstance().find<B>(tag: tag)!;
  final bag = DisposeBag();

  @override
  void onInit() {
    super.onInit();
    try {
      args = Get.arguments as C;
    } catch (e) {
      throw "Missing argument";
    }
  }

  @override
  void dispose() {
    Get.log('"${runtimeType.toString()}" disposed');
    bag.clear();
    super.dispose();
  }
}
