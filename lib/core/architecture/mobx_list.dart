import 'dart:ui';

import 'package:mobx/mobx.dart';

part 'mobx_list.g.dart';

class MobxList<Item> = _MobXListStore with _$MobxList;

abstract class _MobXListStore<Item> with Store {
  @observable
  bool isEmpty = false;
  @observable
  bool isLoading = false;
  @observable
  bool isRefreshing = false;
  @observable
  List<Item> items = <Item>[];

  final Future<List<Item>> Function() getItems;
  VoidCallback? onRefresh;

  _MobXListStore({required this.getItems}) {
    onRefresh = _onRefresh;
  }

  @action
  Future<void> onLoad() async {
    isEmpty = false;
    if (isLoading || isRefreshing) {
      return;
    }
    isLoading = true;
    try {
      final _items = await getItems();
      if (_items.isEmpty) {
        isEmpty = true;
      }
      items = _items;
    } finally {
      isLoading = false;
    }
  }

  @action
  _onRefresh() async {
    isEmpty = false;
    items = [];
    if (isLoading || isRefreshing) {
      return;
    }
    isRefreshing = true;
    try {
      final _items = await getItems();
      if (_items.isEmpty) {
        isEmpty = true;
      }
      items = _items;
    } finally {
      isRefreshing = false;
    }
  }
}
