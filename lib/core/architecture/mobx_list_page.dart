import 'package:flutter/foundation.dart';
import 'package:mobx/mobx.dart';
import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';

part 'mobx_list_page.g.dart';

class MobXListPage<Item> = _MobXListPage with _$MobXListPage;

abstract class _MobXListPage<Item> with Store {
  @observable
  bool isEmpty = false;
  @observable
  bool isLoading = false;
  @observable
  bool isRefreshing = false;
  @observable
  bool isLoadingMore = false;
  @observable
  PagingInfo items = PagingInfo(1, <Item>[], true);
  final Future<PagingInfo<Item>> Function() getItems;
  final Future<PagingInfo<Item>> Function(int) loadMoreItems;
  VoidCallback? onRefresh;

  _MobXListPage({required this.getItems, required this.loadMoreItems}) {
    onRefresh = _onRefresh;
  }

  @action
  Future<void> onLoad() async {
    if (isLoading || isLoadingMore || isRefreshing) {
      return;
    }
    isEmpty = false;
    isLoading = true;
    try {
      final _items = await getItems();
      if (_items.items.isEmpty) {
        isEmpty = true;
      }
      items = _items;
    } finally {
      isLoading = false;
    }
  }

  @action
  Future<void> _onRefresh() async {
    if (isLoading || isLoadingMore || isRefreshing) {
      return;
    }
    isEmpty = false;
    isRefreshing = true;
    try {
      final _items = await getItems();
      if (_items.items.isEmpty) {
        isEmpty = true;
      }
      items = _items;
    } finally {
      isRefreshing = false;
    }
  }

  @action
  Future<void> onLoadMore() async {
    if (isLoading || isLoadingMore || isRefreshing) {
      return;
    }
    final currentItem = items;
    if (currentItem.hasMorePages) {
      isEmpty = false;
      isLoadingMore = true;

      try {
        final newPage = currentItem.page + 1;
        final newItems = await loadMoreItems(newPage);
        items = PagingInfo<Item>(newPage, (currentItem.items + newItems.items) as List<Item>, newItems.hasMorePages);
      } finally {
        isLoadingMore = false;
      }
    }
  }
}
