import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class MobXTableView extends StatefulWidget {
  final int scrollOffset;
  final bool isLoadingMore;
  final bool isRefreshing;
  final bool isEmpty;
  final bool isLoading;
  final VoidCallback? onLoadMore;
  final VoidCallback? onRefresh;
  final Widget child;
  final Widget? loadingIndicator;
  final Widget? reloadingIndicator;
  final Widget? loadingMoreIndicator;
  final Widget? emptyView;

  const MobXTableView({
    Key? key,
    required this.child,
    this.scrollOffset = 0,
    this.onLoadMore,
    this.onRefresh,
    this.isLoadingMore = false,
    this.isRefreshing = false,
    this.isEmpty = false,
    this.emptyView,
    this.isLoading = false,
    this.loadingIndicator,
    this.loadingMoreIndicator,
    this.reloadingIndicator,
  }) : super(key: key);

  @override
  _MobXTableViewState createState() => _MobXTableViewState();
}

class _MobXTableViewState extends State<MobXTableView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(MobXTableView oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.onLoadMore != null && widget.onRefresh != null) {
      return NotificationListener<ScrollNotification>(
        child: RefreshIndicator(
          child: Column(
            children: [
              _buildLoadingIndicator(),
              _buildReLoadingIndicator(),
              _buildEmptyView(),
              Expanded(child: widget.child),
              _buildLoadingMoreIndicator()
            ],
          ),
          onRefresh: () async => widget.onRefresh!(),
        ),
        onNotification: _handleLoadMoreScroll,
      );
    } else if (widget.onRefresh != null) {
      return RefreshIndicator(
        child: Column(
          children: [
            _buildLoadingIndicator(),
            _buildReLoadingIndicator(),
            _buildEmptyView(),
            Expanded(child: widget.child)
          ],
        ),
        onRefresh: () async => widget.onRefresh!(),
      );
    } else if (widget.onLoadMore != null) {
      return NotificationListener<ScrollNotification>(
        child: Observer(builder: (_) {
          return Column(
            children: [
              _buildLoadingIndicator(),
              _buildEmptyView(),
              Expanded(child: widget.child),
              _buildLoadingMoreIndicator()
            ],
          );
        }),
        onNotification: _handleLoadMoreScroll,
      );
    }
    return widget.child;
  }

  Widget _buildLoadingIndicator() {
    return Observer(builder: (_) {
      if (widget.isLoading) {
        if (widget.loadingIndicator == null) {
          return Container(
            width: 55,
            height: 55,
            padding: const EdgeInsets.all(15),
            child: const CircularProgressIndicator(
              strokeWidth: 2,
            ),
          );
        } else {
          return widget.loadingIndicator!;
        }
      }
      return Container();
    });
  }

  Widget _buildReLoadingIndicator() {
    return Observer(builder: (_) {
      if (widget.isRefreshing) {
        if (widget.reloadingIndicator != null) {
          return widget.reloadingIndicator!;
        }
      }
      return Container();
    });
  }

  Widget _buildEmptyView() {
    return Observer(builder: (_) {
      if (widget.isEmpty) {
        return widget.emptyView ?? Container();
      }
      return Container();
    });
  }

  Widget _buildLoadingMoreIndicator() {
    return Observer(builder: (_) {
      if (widget.isLoadingMore) {
        if (widget.loadingMoreIndicator == null) {
          return Container(
            width: 55,
            height: 55,
            padding: const EdgeInsets.all(15),
            child: const CircularProgressIndicator(
              strokeWidth: 2,
            ),
          );
        } else {
          return widget.loadingMoreIndicator!;
        }
      }
      return Container();
    });
  }

  bool _handleLoadMoreScroll(ScrollNotification notification) {
    if (notification is ScrollUpdateNotification) {
      if (notification.metrics.pixels - notification.metrics.maxScrollExtent > widget.scrollOffset) {
        if (widget.onLoadMore != null) {
          if (!widget.isLoadingMore) {
            widget.onLoadMore!();
          }
        }
      }
    }
    return false;
  }
}
