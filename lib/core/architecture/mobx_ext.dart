import 'package:mobx/mobx.dart';

extension MobxObjectExtension<T> on T {
  Observable<T> get obs => Observable<T>(this);

  Observable<T> obsDebug(String name) => Observable<T>(this, name: name);

  ObservableFuture<T> get fObsValue => ObservableFuture<T>.value(this);
}

extension MobxFutureExtension<T> on Future<T> {
  ObservableFuture<T> get fObs => ObservableFuture(this);
}

extension MobxListExtension<T> on List<T> {
  ObservableList<T> get obsList => ObservableList<T>.of(this);
}

extension MobxSetExtension<T> on Set<T> {
  ObservableSet<T> get obsSet => ObservableSet<T>.of(this);
}

extension MobxStreamExtension<T> on Stream<T> {
  ObservableStream<T> get obsStream => ObservableStream<T>(this);
}

extension MobxMapExtension<K, V> on Map<K, V> {
  ObservableMap<K, V> get obsMap => ObservableMap<K, V>.of(this);
}

extension MobxFunctionExtension on Function {
  Action get action => Action(this);

  Action actionDebug(String name) => Action(this, name: name);

  Computed get computed => Computed(this as Function());
}

extension ObservableListExtension<T> on ObservableList<T> {
  void replaceAll(Iterable<T> list) {
    clear();
    addAll(list);
  }

  void replaceAt(int index, T item) {
    this[index] = item;
  }
}
