import 'dart:ui';

// ignore_for_file: non_constant_identifier_names
class Locales {
  static Locale EN = const Locale('en', 'US');
  static Locale JP = const Locale('ja', 'JP');
  static Locale VN = const Locale('vi', 'VN');
}
