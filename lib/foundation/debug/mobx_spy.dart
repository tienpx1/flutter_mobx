import 'dart:developer' as developer;

import 'package:mobx/mobx.dart';

List<String> asyncTasks = [];

void registerMobXSpy() {
  mainContext.config = mainContext.config.clone(
    isSpyEnabled: true,
  );
  mainContext.spy(
    (event) {
      /// Only log start
      if (event.isStart) {
        switch (event.type) {
          case "observable":
            final message = event
                .toString()
                .trim()
                .replaceAll("observable(START)", "Observable|")
                .replaceAll('\n', '|');
            developer.log(message, name: 'MOBX SPY');
            break;
          case "action":
            final message = event
                .toString()
                .trim()
                .replaceAll("action(START)", "Action|")
                .replaceAll('\n', '|');
            if (message.contains("(Zone.run)")) {
              final action = message.replaceAll(r"Action| _", "");
              if (!asyncTasks.contains(action)) {
                developer.log(message, name: 'MOBX SPY');
                asyncTasks = [];
                asyncTasks.add(action);
              }
              break;
            }
            if (!message.contains("(Zone.runUnary)")) {
              developer.log(message, name: 'MOBX SPY');
            }
            break;
          case "reaction":
            final message = event.name
                .trim()
                .replaceAllMapped(RegExp(r'(#.*      )'), (match) => " ")
                .replaceAll('\n', '|');
            developer.log(message, name: 'MOBX SPY');
            break;
          case "reaction-dispose":
            final message = event.name
                .trim()
                .replaceAllMapped(RegExp(r'(#.*      )'), (match) => " ")
                .replaceAll("Observer", "Dispose")
                .replaceAll('\n', '|');
            developer.log(message, name: 'MOBX SPY');
            break;
          default:
            developer.log(event.toString(), name: 'MOBX SPY');
        }
      }
    },
  );
}
