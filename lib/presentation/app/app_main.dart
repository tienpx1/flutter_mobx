import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx_architecture/core/i18n/i18n.dart';
import 'package:flutter_mobx_architecture/foundation/constants.dart';
import 'package:flutter_mobx_architecture/presentation/app/app_pages.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

Future<void> application() async {
  runApp(EasyLocalization(supportedLocales: [
    Locales.VN,
    Locales.EN,
  ], path: kI18nPath, startLocale: Locales.EN, saveLocale: true, child: const MainApp()));
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 780),
      builder: () => GetMaterialApp(
        debugShowCheckedModeBanner: true,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        initialRoute: AppPages.INITIAL,
        getPages: AppPages.routes,
      ),
    );
  }
}
