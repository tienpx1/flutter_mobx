// ignore_for_file: constant_identifier_names
import 'package:flutter/material.dart';
import 'package:flutter_mobx_architecture/presentation/app/app_view.dart';
import 'package:flutter_mobx_architecture/presentation/demo_list/demo_list_view.dart';
import 'package:flutter_mobx_architecture/presentation/demo_load_more/demo_load_more_view.dart';
import 'package:flutter_mobx_architecture/presentation/demo_login/demo_login_view.dart';
import 'package:get/get.dart';

abstract class Routes {
  static const INITIAL = '/';
  static const DEMO_LOGIN = '/demo_login';
  static const DEMO_LOAD_MORE = "/demo_load_more";
  static const DEMO_LIST = '/demo_list';
}

class AppPages {
  static const INITIAL = Routes.INITIAL;

  static final routes = [
    GetPage(
      name: Routes.INITIAL,
      page: () => const AppView(),
      binding: AppBinding(),
    ),
    GetPage(
      name: Routes.DEMO_LOGIN,
      page: () => const DemoLoginView(),
      binding: DemoLoginBinding(),
    ),
    GetPage(
      name: Routes.DEMO_LOAD_MORE,
      page: () => const DemoLoadMoreView(),
      binding: DemoLoadMoreBinding(),
    ),
    GetPage(
      name: Routes.DEMO_LIST,
      page: () => const DemoListView(),
      binding: DemoListBinding(),
    ),
  ];

  static GetPage getPage(String name) {
    return routes.where((s) => s.name == name).first;
  }

  static GetPageRoute generateRoute(String name, RouteSettings routeSettings) {
    final page = getPage(name);
    final routeName = routeSettings.name;
    Get.routing.args = routeSettings.arguments;
    if (routeName == "/") {
      return GetPageRoute(routeName: "/", page: page.page, binding: page.binding);
    } else {
      final child = page.children.where((s) => s.name == routeName).first;
      return GetPageRoute(routeName: child.name, page: child.page, binding: child.binding);
    }
  }
}
