import 'package:flutter/material.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_view.dart';
import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';

import 'app_viewmodel.dart';

class AppBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<AppViewModel>(AppViewModel());
  }
}

class AppView extends MobXView<AppViewModel> {
  const AppView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final items = viewModel.items;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
      ),
      body: Center(
        child: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            final item = items[index];
            return Card(
              shadowColor: Colors.black,
              elevation: 3,
              child: InkWell(
                onTap: () => viewModel.onScenePressed(item.scene),
                child: ListTile(
                  leading: Icon(item.icon),
                  title: Text(item.title),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
