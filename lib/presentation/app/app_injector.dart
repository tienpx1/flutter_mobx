import 'package:flutter_mobx_architecture/data/repositories/app_repository.dart';
import 'package:flutter_mobx_architecture/generated/router.g.dart';
import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';

class AppInjector {
  static void registerDependencies() {
    Get.put(AppRouter(), permanent: true);
    Get.put<AppRepository>(AppRepositoryImpl(), permanent: true);
  }
}
