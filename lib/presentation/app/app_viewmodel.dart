import 'package:flutter/material.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_viewmodel.dart';
import 'package:flutter_mobx_architecture/data/repositories/app_repository.dart';
import 'package:flutter_mobx_architecture/generated/router.g.dart';
import 'package:mobx/mobx.dart';

part 'app_viewmodel.g.dart';

enum ListItemScene { login, demoList, demoListLoadMore }

class ListItem {
  String title;
  IconData icon;
  ListItemScene scene;

  ListItem(this.title, this.icon, this.scene);
}

class AppViewModel = _AppViewModel with _$AppViewModel;

abstract class _AppViewModel extends MobXViewModel<AppRepository, AppRouter> with Store {
  /* Variable */
  final items = [
    ListItem('Login', Icons.login, ListItemScene.login),
    ListItem('Demo List', Icons.list, ListItemScene.demoList),
    ListItem('Demo List With Load More', Icons.list_alt, ListItemScene.demoListLoadMore)
  ];

  /* Computed */
  /* Store */
  /* Life Cycle */
  @override
  void onInit() {
    super.onInit();
  }

  /* Input Action */
  @action
  void onScenePressed(ListItemScene scene) {
    switch (scene) {
      case ListItemScene.login:
        navigator.toDemoLogin();
        break;
      case ListItemScene.demoList:
        navigator.toDemoList();
        break;
      case ListItemScene.demoListLoadMore:
        navigator.toDemoLoadMore();
        break;
    }
  }
}
