import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_list_page_view.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_view.dart';
import 'package:flutter_mobx_architecture/presentation/demo_load_more/demo_load_more_viewmodel.dart';
import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';

class DemoLoadMoreBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<DemoLoadMoreViewModel>(DemoLoadMoreViewModel());
  }
}

class DemoLoadMoreView extends MobXView<DemoLoadMoreViewModel> {
  const DemoLoadMoreView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Demo Load More'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: MobXListPageView(
          store: viewModel.mxList,
          child: Observer(builder: (_) {
            final items = viewModel.mxList.items.items;
            return ListView.separated(
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              itemCount: items.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(items[index].username),
                );
              },
              separatorBuilder: (context, index) {
                return const Divider(thickness: 2);
              },
            );
          }),
        ),
      ),
    );
  }
}
