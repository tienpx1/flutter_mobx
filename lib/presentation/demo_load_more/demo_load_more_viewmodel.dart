import 'package:flutter_mobx_architecture/core/architecture/mobx_list_page.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_viewmodel.dart';
import 'package:flutter_mobx_architecture/data/repositories/app_repository.dart';
import 'package:flutter_mobx_architecture/generated/router.g.dart';
import 'package:flutter_mobx_architecture/models/user.dart';
import 'package:mobx/mobx.dart';

part 'demo_load_more_viewmodel.g.dart';

class DemoLoadMoreViewModel = _DemoLoadMoreViewModel with _$DemoLoadMoreViewModel;

abstract class _DemoLoadMoreViewModel extends MobXViewModel<AppRepository, AppRouter> with Store {
  late MobXListPage<User> mxList;

  @override
  void onInit() {
    super.onInit();
    mxList = MobXListPage<User>(
        getItems: () => repo.getUsers(), loadMoreItems: (pageIndex) => repo.getUsers(page: pageIndex));
    mxList.onLoad();
  }
}
