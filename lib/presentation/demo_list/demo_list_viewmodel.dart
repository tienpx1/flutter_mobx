import 'package:flutter_mobx_architecture/core/architecture/mobx_list.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_viewmodel.dart';
import 'package:flutter_mobx_architecture/data/repositories/app_repository.dart';
import 'package:flutter_mobx_architecture/generated/router.g.dart';
import 'package:flutter_mobx_architecture/models/article.dart';
import 'package:mobx/mobx.dart';

part 'demo_list_viewmodel.g.dart';

class DemoListViewModel = _DemoListViewModel with _$DemoListViewModel;

abstract class _DemoListViewModel extends MobXViewModel<AppRepository, AppRouter> with Store {
  /* Variable */
  late MobxList<Article> mobxList;

  /* Computed */
  /* Store */
  /* Life Cycle */
  @override
  void onInit() async {
    super.onInit();
    mobxList = MobxList<Article>(getItems: () => repo.getNews().then((s) => s.items));
    mobxList.onLoad();
  }

  /* Input Action */
}
