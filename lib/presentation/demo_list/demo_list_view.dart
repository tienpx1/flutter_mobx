import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_list_view.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_view.dart';
import 'package:flutter_mobx_architecture/presentation/demo_list/demo_list_viewmodel.dart';
import 'package:pt_flutter_architecture/pt_flutter_architecture.dart';

class DemoListBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<DemoListViewModel>(DemoListViewModel());
  }
}

class DemoListView extends MobXView<DemoListViewModel> {
  const DemoListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Demo List'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: MobXListView(
          store: viewModel.mobxList,
          child: Observer(
            builder: (item) {
              var demoList = viewModel.mobxList.items;
              return ListView.separated(
                physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                itemCount: demoList.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(demoList[index].title),
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider(thickness: 2);
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
