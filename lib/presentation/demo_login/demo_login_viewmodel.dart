import 'package:flutter/cupertino.dart';
import 'package:flutter_mobx_architecture/core/architecture/future_ext.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_viewmodel.dart';
import 'package:flutter_mobx_architecture/data/repositories/app_repository.dart';
import 'package:flutter_mobx_architecture/generated/router.g.dart';
import 'package:mobx/mobx.dart';

part 'demo_login_viewmodel.g.dart';

class DemoLoginViewModel = _DemoLoginViewModel with _$DemoLoginViewModel;

abstract class _DemoLoginViewModel extends MobXViewModel<AppRepository, AppRouter> with Store {
  /*  Variable */
  final formKey = GlobalKey<FormState>();
  final emailTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  /*  Computed */
  bool get _isValid => formKey.currentState!.validate();

  /*  Store */
  @observable
  bool isLoading = false;

  /*  Life Cycle */
  @override
  void onInit() {
    super.onInit();
    onLoad();
  }

  /*  Input Actions */
  @action
  void onLoad() {}

  @action
  Future<void> onLogin() async {
    if (_isValid) {
      isLoading = true;
      try {
        await delay(200);
        final email = emailTextController.text;
        final password = passwordTextController.text;
        await repo.login(email: email, password: password);
      } finally {
        isLoading = false;
      }
    }
  }
}
