import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_mobx_architecture/core/architecture/mobx_view.dart';
import 'package:get/get.dart';

import 'demo_login_viewmodel.dart';

class DemoLoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<DemoLoginViewModel>(DemoLoginViewModel());
  }
}

class DemoLoginView extends MobXView<DemoLoginViewModel> {
  const DemoLoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login Demo'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [_buildInputs()] + _buildSubmitButtons(),
          ),
        ),
      ),
    );
  }

  Widget _buildInputs() {
    return Form(
      key: viewModel.formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        children: [
          TextFormField(
            decoration: const InputDecoration(labelText: 'Email'),
            controller: viewModel.emailTextController,
            // validator: viewModel.emailValidator,
          ),
          const SizedBox(height: 10),
          TextFormField(
            decoration: const InputDecoration(labelText: 'Password'),
            obscureText: true,
            controller: viewModel.passwordTextController,
            // validator: viewModel.passwordValidator,
          ),
          const SizedBox(height: 5),
        ],
      ),
    );
  }

  List<Widget> _buildSubmitButtons() {
    return <Widget>[
      Observer(builder: (_) {
        if (viewModel.isLoading) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 55,
                height: 55,
                padding: const EdgeInsets.all(15),
                child: const CircularProgressIndicator(
                  strokeWidth: 2,
                ),
              ),
            ],
          );
        } else {
          return ElevatedButton(
            child: const Text('Login', style: TextStyle(fontSize: 20.0)),
            onPressed: viewModel.onLogin,
          );
        }
      })
    ];
  }
}
